1. Tell us what kind of test should be conduct (plan and why) to make sure the page is ok.

Functional dan non Functional testing

Functional testing bertujuan untuk memastikan apakah aplikasi sudah dapat berfungsi sebagaimana seharusnya.
- Misalnya dengan mengklik button yang ada. Apakah hasil yang diperoleh setelah mengklik button tersebut sudah sesuai dengan requirement
- Jika ada checkbox, seharusnya dapat dipilih multiple, dan ketika di submit, nilai-nilai yang sudah dipilih akan disimpan
- Ketika ada Radio button hanya bisa memilih satu saja
- Apakah list di dropdown list muncul semua, dan jika diubah, maka pilihan yang dipilih yang seharusnya ditampilkan dan  value tersebut juga yang harus disimpan ke database
- Field-field yang mandatory harus diberi validasi jika masih kosong, tidak bisa submit form (button Submit inactive)
>> Functional Testing tersebut bertujuan untuk memastikan bahwa semua komponen di halaman/aplikasi tersebut sudah berjalan dan berfungsi sesuai dengan requirement

Non functional juga perlu dilakukan, misalnya stress test. Dilakukan dengan banyak user yang mengakses halaman web tersebut. Tujuannya untuk memastikan bahwa web dapat diakses oleh banyak pengguna pada saat yang bersamaan dan server tidak down

2. Tell us what kind of testcase you want to execute.
Positive testcase, negative testcase, interface testcase and flow testcase

Example:
A. Check User Interface
Step:
- access web: http://devexpress.github.io/testcafe/example/
- Check user interface, should follow the requirement
	including the title

B. Flow I
- Input name on field populate
- Select the Operating System
- Tick on one or any checkbox to select feature
- Select the TestCafe interface on the dropdown list
- Tick on 'I have tried TestCafe'
	Slider rating should be active
- Drag pointer to rate the TestCafe
- Input the 'what you think' about TestCafe
- Click Submit


==========================
READ ME
1. I do the automation using Katalon application
2. Please download the TestingBizzyTeam.rar
3. Unzip and import to Katalon app
4. Run the testcase under Test Cases folder
Thank you